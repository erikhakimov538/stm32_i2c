/**
  ******************************************************************************
  * @file           main.c
  * @brief          Main program body.
  * 				This file contains code of functions for console aplication work.
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "microrl.h"
#include <string.h>
#include <stdio.h>

/* Private variables ---------------------------------------------------------*/
/**
* @cond
*/
ADC_HandleTypeDef hadc;
LCD_HandleTypeDef hlcd;
I2C_HandleTypeDef hi2c1;
UART_HandleTypeDef huart1;
DMA_HandleTypeDef hdma_usart1_rx;
/**
* @endcond
*/

#define _CMD_HELP   	"help"
#define _CMD_DUMP   	"dump"
#define _CMD_DETECT   	"detect"
#define _CMD_GET   		"get"
#define _CMD_SET   		"set"
#define _CMD_CLEAR  	"clear"
#define _NUM_OF_CMD		6

int tx_count;								/** счетчик строки **/
int dev_count = 0;							/** счетчик устройств на шине I2C **/
uint8_t value;								/** значение передаваемое по шине I2C **/
uint8_t data_buf[256];						/** буфер для символов ввода пользователя **/
char * keyword [] = {_CMD_HELP, _CMD_DUMP, _CMD_DETECT, _CMD_GET, _CMD_SET, _CMD_CLEAR};	/** буфер с командами **/
uint8_t text_buf[256];   					/** буфер для вывода ответа польз. **/
uint8_t devices_buf[256];   				/** буфер устройств **/
char * compl_world [_NUM_OF_CMD + 1];		/** значение равно кол-ву команд + 1 **/
char data;

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_ADC_Init(void);
static void MX_LCD_Init(void);
void MX_I2C1_Init(void);
void  master_read(uint16_t device, uint16_t addr);
void  master_write(uint16_t device, uint16_t reg_addr, uint8_t value);
void print_micro (char * str);
int execute (int argc, const char * const * argv);
int getchar_micro(void);
char ** complet (int argc, const char * const * argv);

/** @addtogroup Main
  * @{
  */

/** 
* \~english The main function of the code. The work of the program starts from it, all other functions are called and returned.
* @param None
* @retval None
*
* \~russian Основная функция кода. С нее стартует работа программы, вызываются и возвращаются все остальные функции.
* @param Отсутствует
* @retval Отсутствует
*/
int main(void) {
	microrl_t rl;
	microrl_t * prl = &rl;
	microrl_init (prl, print_micro);
	// set callback for execute
	microrl_set_execute_callback (prl, execute);
	// set callback for completion (optionally)
	microrl_set_complete_callback (prl, complet);

	HAL_Init();
	SystemClock_Config();

	__HAL_RCC_I2C1_CLK_ENABLE();
	HAL_Delay(10);
	__HAL_RCC_I2C1_FORCE_RESET();
	HAL_Delay(10);
	__HAL_RCC_I2C1_RELEASE_RESET();
	HAL_Delay(10);

  /* Initialize all configured peripherals */
	MX_GPIO_Init();
	MX_DMA_Init();
	MX_I2C1_Init();
	MX_USART1_UART_Init();

  while (1) {
	  uint16_t ch = getchar_micro();
	  microrl_insert_char (prl, ch);
  }
}

void transmit(UART_HandleTypeDef * huart, uint8_t * pData, uint16_t Size) {
	HAL_UART_Transmit(huart, pData, Size, 50);
}

/** @addtogroup Initialization
  * @{
  */

/**
  * \~english Initializes and configure system clocks.
  * @param None
  * @retval None
  * @see MX_I2C1_Init
  * @see MX_USART1_UART_Init
  * @see MX_DMA_Init
  * @see MX_GPIO_Init
  *
  *\~russian Инициализация и конфигурация системных часов.
  * @param Отсутствует
  * @retval Отсутствует
  * @see MX_I2C1_Init
  * @see MX_USART1_UART_Init
  * @see MX_DMA_Init
  * @see MX_GPIO_Init
  */

void SystemClock_Config(void) {
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  //Configure the main internal regulator output voltage
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

  //Initializes the RCC Oscillators according to the specified parameters
  //in the RCC_OscInitTypeDef structure.
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI|RCC_OSCILLATORTYPE_LSE;
  RCC_OscInitStruct.LSEState = RCC_LSE_ON;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL6;
  RCC_OscInitStruct.PLL.PLLDIV = RCC_PLL_DIV3;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)  {
    Error_Handler();
  }

  //Initializes the CPU, AHB and APB buses clocks
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_RTC|RCC_PERIPHCLK_LCD;
  PeriphClkInit.RTCClockSelection = RCC_RTCCLKSOURCE_LSE;
  PeriphClkInit.LCDClockSelection = RCC_RTCCLKSOURCE_LSE;

  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK) {
    Error_Handler();
  }
}

/**
  * \~english GPIO initialization function
  * @param None
  * @retval None
  * @see SystemClock_Config
  * @see MX_I2C1_Init
  * @see MX_USART1_UART_Init
  * @see MX_DMA_Init
  *
  * \~russian Инициализация и конфигурация GPIO
  * @param Отсутствует
  * @retval Отсутствует
  * @see SystemClock_Config
  * @see MX_I2C1_Init
  * @see MX_USART1_UART_Init
  * @see MX_DMA_Init
  */

static void MX_GPIO_Init(void) {
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(IDD_CNT_EN_GPIO_Port, IDD_CNT_EN_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, LD4_Pin|LD3_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin : IDD_CNT_EN_Pin */
  GPIO_InitStruct.Pin = IDD_CNT_EN_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(IDD_CNT_EN_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : B1_Pin */
  GPIO_InitStruct.Pin = B1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_EVT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(B1_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : LD4_Pin LD3_Pin */
  GPIO_InitStruct.Pin = LD4_Pin|LD3_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
}

/**
  * \~english I2c bus initialization function
  * @param None
  * @retval None
  * @see SystemClock_Config
  * @see MX_GPIO_Init
  * @see MX_USART1_UART_Init
  * @see MX_DMA_Init
  *
  * \~russian Инициализация и конфигурация шины I2C
  * @param Отсутствует
  * @retval Отсутствует
  * @see SystemClock_Config
  * @see MX_GPIO_Init
  * @see MX_USART1_UART_Init
  * @see MX_DMA_Init
  */

void MX_I2C1_Init(void) {
  hi2c1.Instance = I2C1;
  hi2c1.Init.ClockSpeed = 100000;
  hi2c1.Init.DutyCycle = I2C_DUTYCYCLE_2;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLED;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLED;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLED;
  HAL_I2C_Init(&hi2c1);
}

/**
  * \~english UART initialization function
  * @param None
  * @retval None
  * @see SystemClock_Config
  * @see MX_I2C1_Init
  * @see MX_GPIO_Init
  * @see MX_DMA_Init
  *
  * \~russian Инициализация и конфигурация UARTа
  * @param Отсутствует
  * @retval Отсутствует
  * @see SystemClock_Config
  * @see MX_I2C1_Init
  * @see MX_GPIO_Init
  * @see MX_DMA_Init
  */

void MX_USART1_UART_Init(void) {
	  huart1.Instance = USART1;
	  huart1.Init.BaudRate = 115200;
	  huart1.Init.WordLength = UART_WORDLENGTH_8B;
	  huart1.Init.StopBits = UART_STOPBITS_1;
	  huart1.Init.Parity = UART_PARITY_NONE;
	  huart1.Init.Mode = UART_MODE_TX_RX;
	  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
	  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
	  if (HAL_UART_Init(&huart1) != HAL_OK) {
	    Error_Handler();
	  }
}

/**
  * \~english DMA initialization function
  * @param None
  * @retval None
  * @see SystemClock_Config
  * @see MX_I2C1_Init
  * @see MX_GPIO_Init
  * @see MX_USART1_UART_Init
  *
  * \~russian Инициализация и конфигурация режима DMA
  * @param Отсутствует
  * @retval Отсутствует
  * @see SystemClock_Config
  * @see MX_I2C1_Init
  * @see MX_GPIO_Init
  * @see MX_USART1_UART_Init
  */

void MX_DMA_Init(void) {
  /* DMA controller clock enable */
  __HAL_RCC_DMA1_CLK_ENABLE();

  /* DMA1_Channel5_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel5_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel5_IRQn);
}

/**
  * @}
  */

/** @addtogroup Console
  * @{
  */

/**
* \~english Function of scan user input. 
* @param None
* @retval Scanned input data in uint8_t format.
* @see execute()
*
* \~russian Функция сканирования ввода пользователя.
* @param Отсутствует
* @retval Сканированные данные в uint8_t формате.
* @see execute()
*/

int getchar_micro(void) {
  uint8_t str;
  while(HAL_UART_Receive_IT(&huart1, &str, 1) != HAL_OK) {
  }
//  snprintf(data_buf + strlen(data_buf), 256, &str);
  return str;
}

/** @addtogroup Data
  * @{
  */

/**
* \~english Read byte of data from device on I2C bus
* @param    device number(address) of device on I2C bus.
* @param    addr   register address in device memory.
* @retval   Received data from device.
* @see      master_write()
* @see      i2c_dump()
* 
* \~russian Чтение байта данных из устройства на шине I2C.
* @param    device номер(адрес) устройства на шине I2C.
* @param    addr   адрес регистра в памяти устройства.
* @retval   Данные полученные из устройства
* @see      master_write()
* @see      i2c_dump()
*/

void  master_read(uint16_t device, uint16_t addr) {
		int i;
		uint8_t buffer[] = {addr};
		uint8_t d[2];					//буффер хранящий 16битный адрес и данные для отправки
		d[0] = (addr);    			//младший байт адреса
		d[1] = (addr>>8);       	//старший байт адреса

		for (i = 0; i <= dev_count; i++) {
			if (device == devices_buf[i]) {
        //Возвращение статуса устройства
         while (HAL_I2C_GetState(&hi2c1) != HAL_I2C_STATE_READY) { }
        //Ожидание готовности устройства
        while (HAL_I2C_IsDeviceReady(&hi2c1, (uint16_t)(device<<1), 3, 100) != HAL_OK) { }
        //Отправка данных мастером (i2c handle, i2c address, data buffer pointer, data size, timeout)
            while (HAL_I2C_Master_Transmit(&hi2c1, (uint16_t)(device<<1), (uint8_t *)d, sizeof(d), 1000) != HAL_OK) {
        	if (HAL_I2C_GetError(&hi2c1) != HAL_I2C_ERROR_AF) {
                Error_Handler();
            }
        }
        //Ожидание готовности устройства
        while (HAL_I2C_IsDeviceReady(&hi2c1, (uint16_t)(device<<1), 3, 100) != HAL_OK) {}
        //Получение данных мастером (i2c handle, i2c address, data buffer pointer, data size, timeout)
         value  = 0;
        while (HAL_I2C_Master_Receive(&hi2c1, (uint16_t)(device<<1), (uint8_t *)&value, 1, 100) != HAL_OK) {
            if (HAL_I2C_GetError (&hi2c1) != HAL_I2C_ERROR_AF) {
                Error_Handler();
            }
        }
        tx_count = snprintf(text_buf, 256, "Adr 0x%X - value 0x%X \r\n",  addr, value );
        transmit(&huart1, text_buf, tx_count);
        HAL_Delay(10);
        return value;
			}
		}
	HAL_Delay(10);
	print_micro ("Invalid value, use detect!\r\n");
	HAL_Delay(10);
	return;
}

/**
* \~english Write byte of data to device on I2C bus
* @param    device    number(address) of device on I2C bus.
* @param    reg_addr  register address in device memory.
* @param    value     data to write.
* @retval	None
* @see      master_read()
* @see      i2c_dump()
* 
* \~russian Запись байта данных в память устройства на шине I2C.
* @param    device     номер(адрес) устройства на шине I2C.
* @param    reg_addr   адрес регистра в памяти устройства.
* @param    value      данные для записи.
* @retval	Отсутствует
* @see      master_read()
* @see      i2c_dump()
*/

void master_write(uint16_t device, uint16_t reg_addr, uint8_t value) {
	int i;
	uint8_t count = 0;
    uint8_t d[3];					//буффер хранящий 16битный адрес и данные для отправки
    d[0] = (reg_addr);    			//младший байт адреса
    d[1] = (reg_addr>>8);       	//старший байт адреса
    d[2] = value;                   //данные для записи

    for (i = 0; i <= dev_count; i++) {
    			if (device == devices_buf[i]){
    //ожидание готовности устройства
    		while (HAL_I2C_IsDeviceReady(&hi2c1, (uint16_t)(device<<1), 3, 100) != HAL_OK) {
    			count++;
    		}
    //отправка данных мастером (i2c handle, i2c address, data buffer pointer, data size, timeout)
    		if (HAL_I2C_Master_Transmit(&hi2c1, (uint16_t)(device<<1), (uint8_t *)d, sizeof(d), 1000) != HAL_OK) {
    			Error_Handler();
    		}
    	return;
    	}
    }
   HAL_Delay(10);
   print_micro ("\r\nInvalid value, use detect!");
   HAL_Delay(10);
   return;
}


/**
* \~english Check I2C bus and detect connect devices.
* @retval	None
* @see      master_write()
* @see      i2c_dump()
* @see      master_read()
* 
* \~russian Проверка шины I2C и обнаружение подключенных устройств.
* @retval	Отсутствует
* @see      master_read()
* @see      master_write()
* @see      i2c_dump()
*/

void i2c_detect(void) {
	uint8_t adr = 0;		//счетчик адресов
	dev_count = 0;			//счетчик устройств

	for (adr = 1; adr < 128; adr++) {
		if (HAL_I2C_IsDeviceReady(&hi2c1, (uint16_t)(adr<<1), 3, 100) == HAL_OK) {
			devices_buf[dev_count] = adr;
			tx_count = snprintf(text_buf, 256, "\r\nDevice on adr: 0x%X", adr);
			transmit(&huart1, text_buf, tx_count);
			HAL_Delay(100);
			dev_count++;
		}
	}
	tx_count = snprintf(text_buf, 256, "\r\nDevices on I2C: %d \r\n", dev_count);
    transmit(&huart1, text_buf, tx_count);
    HAL_Delay(500);
}

/**
* \~english Read data from device on I2C bus in registers range.
* @param    dev_adr         number(address) of device on I2C bus.
* @param    cur_reg_adr     number of start register in device memory.
* @param    end_reg_adr     number of end register in device memory.
* @retval	None
* @see      master_read()
* @see      master_write()
* 
* \~russian Чтение данных из устройства на шине I2C в диапазоне регистров.
* @param    dev_adr          номер(адрес) устройства на шине I2C.
* @param    cur_reg_adr      номер стартового регистра в памяти устройства.
* @param    end_reg_adr      номер конечного регистра в памяти устройства.
* @retval	Отсутствует
* @see      master_read()
* @see      master_write()
*/

void i2c_dump(uint16_t dev_adr, uint16_t cur_reg_adr, uint16_t end_reg_adr) {
	int i;
	uint8_t i2c_dump_buf[end_reg_adr];									//буфер прочитанных данных
	int b_count = 0;													//счетчик для записи результата в буффер
	for (i = 0; i <= dev_count; i++) {
	    			if (dev_adr == devices_buf[i]){
	    				while (cur_reg_adr <= (end_reg_adr)) {
	    					master_read(dev_adr, cur_reg_adr);			//чтение байта данных по адресу слейва
	    					i2c_dump_buf[b_count] =  value;				//запись байта в буфер
	    					cur_reg_adr = cur_reg_adr + 0x01;			//обновление адреса
	    					b_count++;									//обновление счетчика
	    				}
	    			return;
	    			}
	    		}
	 HAL_Delay(10);
	 print_micro ("Invalid value, use detect!\r\n");
	 HAL_Delay(10);
	 return;
}

/**
  * @}
  */

/** @addtogroup Print
  * @{
  */

/**
* \~english Print help table for user in console.
* @param    None
* @retval	None
* @see		print_micro()
*
* \~russian Вывод памятки для пользователя в консоль.
* @param    Отсутствует
* @retval	Отсутствует
* @see		print_micro()
*/

void print_help (void) {
	tx_count = snprintf(text_buf, 256, "\r\nUse TAB key for completion\n\rBefore get or set data, nessesary to use detect function!\n\r(All variables takes in hex without 0x)\n\rCommands:\n\r");
	tx_count =  tx_count + snprintf(text_buf + strlen(text_buf), 256, "\tdump (dev_adr, cur_reg, end_reg) - output all device data from memory \n\r");
	tx_count =  tx_count + snprintf(text_buf + strlen(text_buf), 256, "\tdetect (void)			 - detect and display all devices on I2C bus\n\r");
	tx_count =  tx_count + snprintf(text_buf + strlen(text_buf), 256, "\tset (dev_adr, dev_reg, value)	 - sends a byte of data to a device\n\r");
	tx_count =  tx_count + snprintf(text_buf + strlen(text_buf), 256, "\tget (dev_adr, dev_reg)		 - read data byte from device\n\r");
	tx_count =  tx_count + snprintf(text_buf + strlen(text_buf), 256, "\thelp (void)			 - output list of commands\r\n");
	tx_count =  tx_count + snprintf(text_buf + strlen(text_buf), 256, "\tclear (void)			 - clear console window\r\n");
	transmit(&huart1, text_buf, tx_count);
	HAL_Delay(200);
}

/**
  * @}
  */

/**
* \~english Execute callback for microrl library. Causes the matched case to be executed,
*  or displays an error message in case of invalid input from the user.
* @param    argc  number of command words.
* @param    argv  comand words buffer.
* @retval   Execution status.
* @see      print_micro()
* @see      master_read()
* @see      master_write()
* @see      i2c_dump()
*
* \~russian Функция обратного вызова для библиотеки microrl. Вызывает выполнение совпавшего кейса
* или выводит сообщение об ошибке в случае некорректного ввода от пользователя.
* @param    argc  кол-во слов в команде.
* @param    argv  буфер команды.
* @retval   Статус выполнения.
* @see      print_micro()
* @see      master_read()
* @see      master_write()
* @see      i2c_dump()
*/
int execute (int argc, const char * const * argv) {
	int i = 0;
		if (strcmp (argv[i], _CMD_HELP) == 0) {
			print_help ();        							// если совпадение с _CMD_HELP -  вывод памятки
		} else if (strcmp (argv[i], _CMD_CLEAR) == 0) {		// если совпадение с _CMD_CLEAR -  очистка ввода
			HAL_Delay(10);
			tx_count = snprintf(text_buf, 256, "\033[2J");
			transmit(&huart1, text_buf, tx_count);
			HAL_Delay(10);
			tx_count = snprintf(text_buf, 256, "\033[H");
			transmit(&huart1, text_buf, tx_count);
			HAL_Delay(10);
		} else if (strcmp (argv[i], _CMD_DETECT) == 0) {
			i2c_detect();									// если совпадение с _CMD_DETECT - вывод списка устр
			HAL_Delay(10);
		} else if (strcmp (argv[i], _CMD_DUMP) == 0) {		// если совпадение с _CMD_DUMP - вызов функции i2c_dump
			if (argc == 4) {								// проверка значений на пустоту
				uint32_t d_adr, cr_adr, er_adr;				// адрес девайса, стартовый адрес памяти, конечный адрес памяти
				HAL_Delay(10);
				print_micro ("\r\n");
				HAL_Delay(10);
				sscanf(argv[i+3], "%X", &er_adr);			//обработка данных
				sscanf(argv[i+2], "%X", &cr_adr);			//обработка данных
				sscanf(argv[i+1], "%X", &d_adr);			//обработка данных
				i2c_dump(d_adr, cr_adr, er_adr);			// вызов функции с обработанными данными
				return 0;
			} else {
				memset(text_buf, 0, sizeof(text_buf));
				HAL_Delay(10);
				tx_count = snprintf(text_buf, 256, "\n\rInvalid values\n\r");
				transmit(&huart1, text_buf, tx_count);
				HAL_Delay(10);
				return 1;
			}
		} else if (strcmp (argv[i], _CMD_SET) == 0) {		// если совпадение с _CMD_SET - вызов функции записи в устр.
			if (argc == 4) {								// проверка значений на пустоту
				uint32_t r_addr, val, d_adr;
				sscanf(argv[i+3], "%X", &val);				//обработка данных
				sscanf(argv[i+2], "%X", &r_addr);			//обработка данных
				sscanf(argv[i+1], "%X", &d_adr);			//обработка данных
				master_write(d_adr, r_addr, val);			// вызов функции с обработанными данными
				HAL_Delay(10);
				print_micro ("\r\n");
				HAL_Delay(10);
				return 0;
			} else {
					HAL_Delay(10);
					print_micro ("\r\nInvalid values\r\n");	// предупреждение о пустых значениях
					HAL_Delay(10);
				return 1;
			}
		} else if (strcmp (argv[i], _CMD_GET) == 0) {		// если совпадение с _CMD_GET - вызов функции чтения из устр.
			if (argc == 3) {								// проверка значений на пустоту
				uint32_t r_addr, device;					// адрес регистра памяти
				HAL_Delay(10);
				print_micro ("\r\n");
				HAL_Delay(10);
				sscanf(argv[i+1], "%X", &device);			//обработка данных
				sscanf(argv[i+2], "%X", &r_addr);			//обработка данных
				master_read(device, r_addr);				// вызов функции с обработанными данными
				return 0;
			} else {
					HAL_Delay(10);
					print_micro ("\r\nInvalid values\n\r");	// предупреждение о пустых значениях
					HAL_Delay(10);
				return 1;
			}
		} else {
			HAL_Delay(10);
			print_micro ("\n\rCommand not found!\n\r");
			HAL_Delay(10);
		}
	return 0;
}

/** @addtogroup Print
  * @{
  */

/**
* \~english Print data in console.
* @param    str data for ptint.
* @retval	None
* @see      complet()
* @see		print_help()
* 
* \~russian Вывод данных в консоль.
* @param    str данные для вывода.
* @retval	Отсутствует
* @see      complet()
* @see		print_help()
*/

void print_micro (char * str) {
		  int count;
		  memset(text_buf, 0, sizeof(text_buf));
		  count = snprintf(text_buf, 256, "%s", str);
		  transmit(&huart1, str, strlen(str));
}

/**
  * @}
  */

/**
* \~english Completion command(word) after press TAB button.
* @param    argc     number of input words to complete.
* @param    argv     input words buffer.
* @retval   complete command word
* @see      print_micro()
* 
* \~russian Автодополнение команды по нажатию кнопки TAB.
* @param    argc     кол-во введеных слов команды.
* @param    argv     буфер с введеными словами.
* @retval   Дополненное слово.
* @see      print_micro()
*/

char ** complet (int argc, const char * const * argv) {
	int j = 0;
	compl_world [0] = 0;
	if (argc == 1) {
		// последний введеный символ
		char * bit = (char*)argv [argc-1];
		// проверка на совпадение с массивом команд
		for (int i = 0; i < _NUM_OF_CMD; i++) {
			// при совпадении дописать команду
			if (strstr(keyword [i], bit) == keyword [i]) {
				compl_world [j++] = keyword [i];
			}
		}

	compl_world [j] = NULL;
	// возвращение полученного значения
	return compl_world;
	}
}
/**
  * @}
  */

/** @addtogroup Handlers
  * @{
  */

/**
  * \~english  This function is executed in case of error occurrence.
  * @param	None
  * @retval None
  *
  * \~russian	Эта функция выполняется в случае возникновения ошибки.
  * @param 	Отсутствует
  * @retval Отсутствует
  */

void Error_Handler(void) {
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1) {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT

/**
  * \~english  Reports the name of the source file and the source line number
  *            where the assert_param error has occurred.
  *	@param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  *
  * \~russian	Сообщает имя исходного файла и номер исходной строки
  * , где произошла ошибка assert_param.
  * @param	file: указатель на имя исходного файла
  * @param	line: номер источника строки ошибки assert_param
  * @retval Отсутствует
  */

void assert_failed(uint8_t *file, uint32_t line) {
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/**
  * @}
  */

/**
  * @}
  */

