# Short description of commands # {#mainpage}
This project is console application for monitoring devices on I2C bus. 
Include list of commands:
1. help - output short prompt for user;
2. detect - scan I2C bus for connection devices;
3. dump - read bytes of data from device on I2C bus;
4. set - write byte of data to device on I2C bus;
5. get - read byte of data from device on I2C bus;
6. clear - clear console window.

Also project use ``microrl`` library to be able to use full console features such as TAB button to complete the command,
 history of input commands and ANSI ESC codes.

## Build
This project have automatic build script ```Makefile```. For build this project, you need just write one command in console (for clean make directory and some related files you need command ```make clean```).

1. Go to project path:
    ```cd <project_path>```

2. Copy this repo to your computer.

3. Go to build folder:
    ```cd build```
    (If the folder does not exist, it must be created: ```mkdir build```)

4. Open project in your IDE and write to console (you can also use system console): ``cmake .. -G "Unix Makefiles" -DCMAKE_TOOLCHAIN_FILE=../arm-none-eabi-gcc.cmake``

5. After generate write: ```make```


# Краткое описание команд  <!--- # {#mainpage} -->
Этот проект представляет собой консольное приложение для мониторинга устройств на шине I2C.
Включает в себя следующий список команд:
1. help - выводит короткую подсказку для пользователя;
2. detect - сканирование шины I2C на предмет подключения устройств;
3. dump(dev_adr, cur_reg, end_reg) - считать байты данных с устройства по шине I2C, dev_adr - адрес устройства на шине, cur_reg - начальный адрес чтения, end_reg - конечный адрес чтения (команда считывает данные со всех адресов включая начальный и конечный);
4. set (dev_adr, dev_reg, value) - записать байт данных в устройство по шине I2C, dev_ad - адрес устройства на шине, dev_reg - регистр устройства в который осуществляется запись, value - записываемое значение;
5. get (dev_adr, dev_reg) - прочитать байт данных с устройства по шине I2C, dev_adr - адрес устройства на шине, dev_reg - регистр устройства из которого читаются данные;
6. clear - очистить окно консоли.

Все адреса, значения и аргументы команд пишутся без 0x в HEX формате!!!

Также проект использует библиотеку ``microrl``, чтобы иметь возможность использовать все функции консоли, такие как кнопка TAB для выполнения команды, история вводимых команд и кодов ANSI ESC.

## Сборка
Этот проект имеет скрипт автоматической сборки Makefile.

1. Перейдите в будущую директорию проекта: 
	``cd <путь_проекта>``

2. Скопируйте этот репозиторий на свой компьютер: 
	``git clone https://gitlab.com/erikhakimov538/stm32_i2c.git``

3. Перейдите в папку сборки: 
	``cd build``
     (Если папка не существует, ее необходимо создать: ``mkdir build``)

4. Откройте проект в вашей среде IDE и напишите в консоли (вы также можете использовать системную консоль): 
	``cmake .. -G "Unix Makefiles" -DCMAKE_TOOLCHAIN_FILE=../arm-none-eabi-gcc.cmake``

5. После процесса генерации прописать команду: 
	``make``
	
## Прошивка через утилиту st-flash

1. Переходим в проекте в папку build;

2. Запускаем сборку командой:
	``make``;
	
3. Очищаем память платы командой: 
	``st-flash erase``; 
	
4. Загружаем прошивку командой:
	``st-flash --reset write stm32.bin 0x08000000``;
	
5. После этого перезагружаем устройство и проверяем работу.	
